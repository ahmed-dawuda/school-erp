<!DOCTYPE html>
<html>
<head>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>
        @yield('titleText')
    </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset('custom/components/bootstrap/dist/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('custom/components/font-awesome/css/font-awesome.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{asset('custom/components/Ionicons/css/ionicons.min.css')}}">

    {{--Additional CSS files for children--}}
            @yield('css')
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('custom/dist/css/AdminLTE.min.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{asset('custom/dist/css/skins/_all-skins.min.css')}}">

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
        .btn, .box{
            border-radius: 0px;
        }
    </style>
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-{{env("THEME", "skin-red")}} layout-top-nav">
<div class="wrapper">

    <header class="main-header">
        <nav class="navbar navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <a href="#" class="navbar-brand"><b>{{strtoupper(env("SCHOOL_NAME", "St. Pauls"))}}</b></a>
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                        <i class="fa fa-bars"></i>
                    </button>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse pull-left" id="navbar-collapse">

                    <ul class="nav navbar-nav">

                        <li>
                            <a href="{{route('welcome')}}"> <span class="fa fa-home"></span> Home </a>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                About Us <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu" >
                                <li><a href="{{route('history')}}">History, Vision & Policy</a></li>
                                <li><a href="{{route('campuses')}}">Campuses</a></li>
                                <li><a href="{{route('map')}}">Maps & Directions</a></li>
                                <li><a href="{{route('students')}}">Student Leaders</a></li>
                                {{--<li class="divider"></li>--}}
                                <li><a href="{{route('staff')}}">Staff</a></li>
                                {{--<li class="divider"></li>--}}
                                <li><a href="{{route('alumni')}}">Notable Alumni</a></li>
                                <li><a href="{{route('academics')}}">Academics</a></li>
                                {{--<li><a href="{{route('network')}}">Our Network and Affiliations</a></li>--}}
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                Admissions <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{route('admission', ['level'=> 'kindergarten'])}}">Kindergarten</a></li>
                                <li><a href="{{route('admission', ['level'=> 'primary'])}}">Primary</a></li>
                                <li><a href="{{route('admission', ['level'=> 'junior-high'])}}">Junior High</a></li>
                                {{--<li class="divider"></li>--}}
                                <li><a href="{{route('admission', ['level'=> 'senior-high'])}}">Senior High</a></li>
                                {{--<li class="divider"></li>--}}
                                {{--<li><a href="#">One more separated link</a></li>--}}
                            </ul>
                        </li>

                        {{--<li>--}}
                            {{--<a href="#">Network</a>--}}
                        {{--</li>--}}

                        <li>
                            <a href="{{route('donation')}}">Donations</a>
                        </li>

                        <li>
                            <a href="{{route('library')}}"> <span class="fa fa-book"></span> Library </a>
                        </li>

                        <li>
                            <a href="{{route('questions')}}">Question Bank</a>
                        </li>

                        <li>
                            <a href="http://pbskids.org/games/" target="_blank">Games</a>
                        </li>

                        <li>
                            <a href="{{route('download')}}">Download</a>
                        </li>
                    </ul>

                </div>
                <!-- /.navbar-collapse -->
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->
                        <li class="dropdown messages-menu">
                            <!-- Menu toggle button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <span class="fa fa-search"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">Search our site</li>
                                    <form class="navbar-form navbar-left" role="search">
                                        <div class="input-group margin">
                                            <input type="text" class="form-control text-danger">
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-{{env("SEARCH_BUTTON", "danger")}} btn-flat">
                                                        <span class="fa fa-search"></span>
                                                    </button>
                                                </span>
                                        </div>
                                    </form>
                            </ul>
                        </li>
                        <!-- /.messages-menu -->

                        <!-- Notifications Menu -->
                        <li class="dropdown notifications-menu">
                            <!-- Menu toggle button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                Events
                                <span class="label label-warning">10</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">You have 10 notifications</li>
                                <li>
                                    <!-- Inner Menu: contains the notifications -->
                                    <ul class="menu">
                                        <li><!-- start notification -->
                                            <a href="#">
                                                <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                            </a>
                                        </li>
                                        <!-- end notification -->
                                    </ul>
                                </li>
                                <li class="footer"><a href="#">View all</a></li>
                            </ul>
                        </li>
                        <!-- Tasks Menu -->
                        @if(!Auth::check())
                            <li>
                                <a href="#" data-toggle="modal" data-target="#modal-default">
                                    <span class="fa fa-unlock"></span>
                                    Student login
                                </a>
                            </li>
                        @else
                        <li>
                            <a href="#">
                                <span class="fa fa-cogs"></span>
                                Portal
                            </a>
                        </li>
                        @endif

                    </ul>
                </div>
                <!-- /.navbar-custom-menu -->
            </div>
            <!-- /.container-fluid -->
        </nav>
    </header>
    <!-- Full Width Column -->
    <div class="content-wrapper">
        <div class="container">
            <!-- Content Header (Page header) -->
            @yield('carousel')

            <section class="content-header">

               @yield('header_bread')

            </section>

            <!-- Main content -->
            {{--<section class="content">--}}
                @yield('content')
            {{--</section>--}}
            <!-- /.content -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="container">
            <div class="pull-right hidden-xs">
                <b>Version</b> 2.4.0
            </div>
            <strong>Copyright &copy; 2014-2016 <a href="#">University of Computer Science</a>.</strong> All rights
            reserved.
        </div>
        <!-- /.container -->
    </footer>
</div>
<!-- ./wrapper -->
@if(!Auth::check())
    @include('pages.home.loginModal')
@endif
<!-- jQuery 3 -->
<script src="{{asset('custom/components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('custom/components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{asset('custom/components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>


<!-- FastClick -->
<script src="{{asset('custom/components/fastclick/lib/fastclick.js')}}"></script>

{{--Additional js files for children--}}
@yield('js')

<!-- AdminLTE App -->
<script src="{{asset('custom/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
{{--<script src="{{asset('custom/dist/js/demo.js')}}"></script>--}}

<script>
</script>
</body>
</html>
