@extends('layouts.master')
@section('titleText')
    Download our apps
@endsection
@section('header_bread')
    <h1 class="text-{{env('THEME')}}">
        Parents Portal App
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{route('welcome')}}"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Download</li>
    </ol>
    <br>
@endsection
@section('content')
    <div class="row clearfix">
        <div class="col-md-6">
            <a href="#">
                <div class="info-box bg-gray">
                    <span class="info-box-icon"><i class="fa fa-apple"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">iOS</span>
                        <span class="info-box-number">Parents Portal App</span>

                        <div class="progress">
                            <div class="progress-bar" style="width: 50%"></div>
                        </div>
              <span class="progress-description">
                    237+ downloads
                  </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </a>
        </div>
        <div class="col-md-6">
            <a href="#">
                <div class="info-box bg-green-active">
                    <span class="info-box-icon"><i class="fa fa-android"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">android</span>
                        <span class="info-box-number">Parents Portal App</span>

                        <div class="progress">
                            <div class="progress-bar" style="width: 20%"></div>
                        </div>
              <span class="progress-description">
                    692+ downloads
                  </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </a>
        </div>
    </div>

    <section class="content-header">
        <h1 class="text-{{env('THEME')}}">
            Teachers Portal App
        </h1>
    </section>
    <br>
    <div class="row clearfix">
        <div class="col-md-6">
            <a href="#">
                <div class="info-box bg-gray">
                    <span class="info-box-icon"><i class="fa fa-apple"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">iOS</span>
                        <span class="info-box-number">Teachers Portal App</span>

                        <div class="progress">
                            <div class="progress-bar" style="width: 50%"></div>
                        </div>
              <span class="progress-description">
                    237+ downloads
                  </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </a>
        </div>
        <div class="col-md-6">
            <a href="#">
                <div class="info-box bg-green-active">
                    <span class="info-box-icon"><i class="fa fa-android"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">android</span>
                        <span class="info-box-number">Teachers Portal App</span>

                        <div class="progress">
                            <div class="progress-bar" style="width: 20%"></div>
                        </div>
              <span class="progress-description">
                    692+ downloads
                  </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </a>
        </div>
    </div>

@endsection