@extends('layouts.master')
@section('titleText')
    Academic Performance
@endsection
@section('header_bread')
    <h1 class="text-{{env('THEME')}}">
        Academic Performances here at {{env('SCHOOL_NAME')}}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{route('welcome')}}"><i class="fa fa-home"></i> Home</a></li>
        <li>About</li>
        <li class="active">Academics</li>
    </ol>
    <br>
@endsection
@section('content')

@endsection