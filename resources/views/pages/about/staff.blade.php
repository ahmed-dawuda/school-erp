@extends('layouts.master')
@section('titleText')
    Staff at {{env('SCHOOL_NAME')}}
@endsection
@section('css')

@endsection
@section('header_bread')
    <h1 class="text-{{env('THEME')}}">
        Teaching & non-teaching staff
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{route('welcome')}}"><i class="fa fa-home"></i> Home</a></li>
        <li>About</li>
        <li class="active">Staff</li>
    </ol>
    <br>
@endsection
@section('content')

    <div class="box box-{{env('SEARCH_BUTTON')}}">
        <div class="box-header with-border">
            <h3 class="box-title">Senior High</h3>
            <div class="box-tools pull-right">
                <div class="form-group">
                    <select class="form-control">
                        <option>Select Campus</option>
                        <option>Achimota</option>
                        <option>Asafo</option>
                        <option>North-Ridge</option>
                    </select>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
            <ul class="users-list clearfix">
                <li>
                    <img src="{{asset('custom/dist/img/user1-128x128.jpg')}}" alt="User Image">
                    <a class="users-list-name" href="#">Alexander Pierce</a>
                    <span class="users-list-date">Today</span>
                </li>
                <li>
                    <img src="{{asset('custom/dist/img/user8-128x128.jpg')}}" alt="User Image">
                    <a class="users-list-name" href="#">Norman</a>
                    <span class="users-list-date">Yesterday</span>
                </li>
                <li>
                    <img src="{{asset('custom/dist/img/user7-128x128.jpg')}}" alt="User Image">
                    <a class="users-list-name" href="#">Jane</a>
                    <span class="users-list-date">12 Jan</span>
                </li>
                <li>
                    <img src="{{asset('custom/dist/img/user6-128x128.jpg')}}" alt="User Image">
                    <a class="users-list-name" href="#">John</a>
                    <span class="users-list-date">12 Jan</span>
                </li>
                <li>
                    <img src="{{asset('custom/dist/img/user2-160x160.jpg')}}" alt="User Image">
                    <a class="users-list-name" href="#">Alexander</a>
                    <span class="users-list-date">13 Jan</span>
                </li>
                <li>
                    <img src="{{asset('custom/dist/img/user5-128x128.jpg')}}" alt="User Image">
                    <a class="users-list-name" href="#">Sarah</a>
                    <span class="users-list-date">14 Jan</span>
                </li>
                <li>
                    <img src="{{asset('custom/dist/img/user4-128x128.jpg')}}" alt="User Image">
                    <a class="users-list-name" href="#">Nora</a>
                    <span class="users-list-date">15 Jan</span>
                </li>
                <li>
                    <img src="{{asset('custom/dist/img/user3-128x128.jpg')}}" alt="User Image">
                    <a class="users-list-name" href="#">Nadia</a>
                    <span class="users-list-date">15 Jan</span>
                </li>
            </ul>
            <!-- /.users-list -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer text-center">
            <a href="javascript:void(0)" class="uppercase">View All Users</a>
        </div>
        <!-- /.box-footer -->
    </div>

    <div class="box box-{{env('SEARCH_BUTTON')}}">
        <div class="box-header with-border">
            <h3 class="box-title">Junior High</h3>
            <div class="box-tools pull-right">
                <div class="form-group">
                    <select class="form-control">
                        <option>Select Campus</option>
                        <option>Achimota</option>
                        <option>Asafo</option>
                        <option>North-Ridge</option>
                    </select>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
            <ul class="users-list clearfix">
                <li>
                    <img src="{{asset('custom/dist/img/user1-128x128.jpg')}}" alt="User Image">
                    <a class="users-list-name" href="#">Alexander Pierce</a>
                    <span class="users-list-date">Today</span>
                </li>
                <li>
                    <img src="{{asset('custom/dist/img/user8-128x128.jpg')}}" alt="User Image">
                    <a class="users-list-name" href="#">Norman</a>
                    <span class="users-list-date">Yesterday</span>
                </li>
                <li>
                    <img src="{{asset('custom/dist/img/user7-128x128.jpg')}}" alt="User Image">
                    <a class="users-list-name" href="#">Jane</a>
                    <span class="users-list-date">12 Jan</span>
                </li>
                <li>
                    <img src="{{asset('custom/dist/img/user6-128x128.jpg')}}" alt="User Image">
                    <a class="users-list-name" href="#">John</a>
                    <span class="users-list-date">12 Jan</span>
                </li>
                <li>
                    <img src="{{asset('custom/dist/img/user2-160x160.jpg')}}" alt="User Image">
                    <a class="users-list-name" href="#">Alexander</a>
                    <span class="users-list-date">13 Jan</span>
                </li>
                <li>
                    <img src="{{asset('custom/dist/img/user5-128x128.jpg')}}" alt="User Image">
                    <a class="users-list-name" href="#">Sarah</a>
                    <span class="users-list-date">14 Jan</span>
                </li>
                <li>
                    <img src="{{asset('custom/dist/img/user4-128x128.jpg')}}" alt="User Image">
                    <a class="users-list-name" href="#">Nora</a>
                    <span class="users-list-date">15 Jan</span>
                </li>
                <li>
                    <img src="{{asset('custom/dist/img/user3-128x128.jpg')}}" alt="User Image">
                    <a class="users-list-name" href="#">Nadia</a>
                    <span class="users-list-date">15 Jan</span>
                </li>
            </ul>
            <!-- /.users-list -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer text-center">
            <a href="javascript:void(0)" class="uppercase">View All Users</a>
        </div>
        <!-- /.box-footer -->
    </div>

    <div class="box box-{{env('SEARCH_BUTTON')}}">
        <div class="box-header with-border">
            <h3 class="box-title">Primary School</h3>
            <div class="box-tools pull-right">
                <div class="form-group">
                    <select class="form-control">
                        <option>Select Campus</option>
                        <option>Achimota</option>
                        <option>Asafo</option>
                        <option>North-Ridge</option>
                    </select>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
            <ul class="users-list clearfix">
                <li>
                    <img src="{{asset('custom/dist/img/user1-128x128.jpg')}}" alt="User Image">
                    <a class="users-list-name" href="#">Alexander Pierce</a>
                    <span class="users-list-date">Today</span>
                </li>
                <li>
                    <img src="{{asset('custom/dist/img/user8-128x128.jpg')}}" alt="User Image">
                    <a class="users-list-name" href="#">Norman</a>
                    <span class="users-list-date">Yesterday</span>
                </li>
                <li>
                    <img src="{{asset('custom/dist/img/user7-128x128.jpg')}}" alt="User Image">
                    <a class="users-list-name" href="#">Jane</a>
                    <span class="users-list-date">12 Jan</span>
                </li>
                <li>
                    <img src="{{asset('custom/dist/img/user6-128x128.jpg')}}" alt="User Image">
                    <a class="users-list-name" href="#">John</a>
                    <span class="users-list-date">12 Jan</span>
                </li>
                <li>
                    <img src="{{asset('custom/dist/img/user2-160x160.jpg')}}" alt="User Image">
                    <a class="users-list-name" href="#">Alexander</a>
                    <span class="users-list-date">13 Jan</span>
                </li>
                <li>
                    <img src="{{asset('custom/dist/img/user5-128x128.jpg')}}" alt="User Image">
                    <a class="users-list-name" href="#">Sarah</a>
                    <span class="users-list-date">14 Jan</span>
                </li>
                <li>
                    <img src="{{asset('custom/dist/img/user4-128x128.jpg')}}" alt="User Image">
                    <a class="users-list-name" href="#">Nora</a>
                    <span class="users-list-date">15 Jan</span>
                </li>
                <li>
                    <img src="{{asset('custom/dist/img/user3-128x128.jpg')}}" alt="User Image">
                    <a class="users-list-name" href="#">Nadia</a>
                    <span class="users-list-date">15 Jan</span>
                </li>
            </ul>
            <!-- /.users-list -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer text-center">
            <a href="javascript:void(0)" class="uppercase">View All Users</a>
        </div>
        <!-- /.box-footer -->
    </div>
@endsection