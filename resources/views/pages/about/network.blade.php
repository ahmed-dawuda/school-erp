@extends('layouts.master')
@section('titleText')
    Our Networks and Affiliations
@endsection
@section('header_bread')
    <h1 class="text-{{env('THEME')}}">
        Our Networks & Affiliations
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{route('welcome')}}"><i class="fa fa-home"></i> Home</a></li>
        <li>About</li>
        <li class="active">Networks & Affiliations</li>
    </ol>
    <br>
@endsection
@section('content')

@endsection