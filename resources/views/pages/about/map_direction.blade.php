@extends('layouts.master')
@section('titleText')
    Maps & Directions to {{env('SCHOOL_NAME')}}
@endsection
@section('header_bread')
    <h1 class="text-{{env('THEME')}}">
        Maps & Directions
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{route('welcome')}}"><i class="fa fa-home"></i> Home</a></li>
        <li>About</li>
        <li class="active">Maps & Directions</li>
    </ol>
    <br>
@endsection
@section('content')
    <iframe allowfullscreen="" frameborder="0" height="400px"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3962.7880416418434!2d-1.5675901845158762!3d6.67316442325497!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xfdb9461ea6fb9af%3A0xb0da4f6d9c4b361f!2sKwame+Nkrumah+University+of+Science+and+Technology+(KNUST)!5e0!3m2!1sen!2sgh!4v1505975873778"
            style="border:0" width="100%">
    </iframe>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-{{env('SEARCH_BUTTON')}}">
                <div class="box-header with-border">
                    <h3 class="box-title">Contact Us</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="row clearfix">
                    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">

                    </div>
                    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                        <form role="form">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Full Name</label>
                                    <input type="text" class="form-control"  placeholder="Enter full name">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email address</label>
                                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                                </div>
                                <div class="form-group">
                                    <label>Message</label>
                                    <textarea class="form-control" rows="3" placeholder="Type your message..."></textarea>
                                </div>

                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" checked> Send me news and events
                                    </label>
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-{{env('SEARCH_BUTTON')}}">Send message</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection