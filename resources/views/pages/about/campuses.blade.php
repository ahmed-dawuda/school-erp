@extends('layouts.master')
@section('titleText')
    Compuses at {{env('SCHOOL_NAME')}}
@endsection
@section('header_bread')
        <h1 class="text-{{env('THEME')}}">
            Our Campuses
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('welcome')}}"><i class="fa fa-home"></i> Home</a></li>
            <li>About</li>
            <li class="active">Campuses</li>
        </ol>
        <br>
@endsection
@section('content')
   <div class="row clearfix box box-solid">
       <div class="box-body">
           <div class="box-header with-border">
               <h3>Achimota</h3>
           </div>
           <div class="col-md-6">
               <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                   <ol class="carousel-indicators">
                       <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                       <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                       <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                   </ol>
                   <div class="carousel-inner">
                       <div class="item active">
                           <img src="http://placehold.it/900x500/39CCCC/ffffff&text=I+Love+Bootstrap" alt="First slide">

                           <div class="carousel-caption">
                               First Slide
                           </div>
                       </div>
                       <div class="item">
                           <img src="http://placehold.it/900x500/3c8dbc/ffffff&text=I+Love+Bootstrap" alt="Second slide">

                           <div class="carousel-caption">
                               Second Slide
                           </div>
                       </div>
                       <div class="item">
                           <img src="http://placehold.it/900x500/f39c12/ffffff&text=I+Love+Bootstrap" alt="Third slide">

                           <div class="carousel-caption">
                               Third Slide
                           </div>
                       </div>
                   </div>
                   <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                       <span class="fa fa-angle-left"></span>
                   </a>
                   <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                       <span class="fa fa-angle-right"></span>
                   </a>
               </div>
           </div>
           <div class="col-md-6" style="font-size: 16px;">
               Sed ut perspiciatis unde omnis iste
               natus error sit voluptatem accusantium
               doloremque laudantium, totam rem aperiam,
               eaque ipsa quae ab illo inventore veritatis et
               quasi architecto beatae vitae dicta sunt explicabo.
               Nemo enim ipsam voluptatem quia voluptas sit aspernatur
               aut odit aut fugit, sed quia consequuntur magni dolores
               eos qui ratione voluptatem sequi nesciunt. Neque porro
               quisquam est, qui dolorem ipsum quia dolor sit amet,
               consectetur, adipisci velit, sed quia non numquam eius
               modi tempora incidunt ut labore et dolore magnam aliquam
               quaerat voluptatem. Ut enim ad minima veniam, quis nostrum
               exercitationem ullam corporis suscipit laboriosam, nisi ut
               aliquid ex ea commodi consequatur? Quis autem vel eum iure
               reprehenderit qui in ea voluptate velit esse quam nihil molestiae
               consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?
           </div>
           <br>
       </div>
   </div>

   <div class="row clearfix box box-solid">
       <div class="box-body">
           <div class="box-header with-border">
               <h3>Asafo</h3>
           </div>
           <div class="col-md-6">
               <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                   <ol class="carousel-indicators">
                       <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                       <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                       <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                   </ol>
                   <div class="carousel-inner">
                       <div class="item active">
                           <img src="http://placehold.it/900x500/39CCCC/ffffff&text=I+Love+Bootstrap" alt="First slide">

                           <div class="carousel-caption">
                               First Slide
                           </div>
                       </div>
                       <div class="item">
                           <img src="http://placehold.it/900x500/3c8dbc/ffffff&text=I+Love+Bootstrap" alt="Second slide">

                           <div class="carousel-caption">
                               Second Slide
                           </div>
                       </div>
                       <div class="item">
                           <img src="http://placehold.it/900x500/f39c12/ffffff&text=I+Love+Bootstrap" alt="Third slide">

                           <div class="carousel-caption">
                               Third Slide
                           </div>
                       </div>
                   </div>
                   <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                       <span class="fa fa-angle-left"></span>
                   </a>
                   <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                       <span class="fa fa-angle-right"></span>
                   </a>
               </div>
           </div>
           <div class="col-md-6" style="font-size: 16px;">
               Sed ut perspiciatis unde omnis iste
               natus error sit voluptatem accusantium
               doloremque laudantium, totam rem aperiam,
               eaque ipsa quae ab illo inventore veritatis et
               quasi architecto beatae vitae dicta sunt explicabo.
               Nemo enim ipsam voluptatem quia voluptas sit aspernatur
               aut odit aut fugit, sed quia consequuntur magni dolores
               eos qui ratione voluptatem sequi nesciunt. Neque porro
               quisquam est, qui dolorem ipsum quia dolor sit amet,
               consectetur, adipisci velit, sed quia non numquam eius
               modi tempora incidunt ut labore et dolore magnam aliquam
               quaerat voluptatem. Ut enim ad minima veniam, quis nostrum
               exercitationem ullam corporis suscipit laboriosam, nisi ut
               aliquid ex ea commodi consequatur? Quis autem vel eum iure
               reprehenderit qui in ea voluptate velit esse quam nihil molestiae
               consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?
           </div>
           <br>
       </div>
   </div>

   <div class="row clearfix box box-solid">
       <div class="box-body">
           <div class="box-header with-border">
               <h3>North-Ridge</h3>
           </div>
           <div class="col-md-6">
               <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                   <ol class="carousel-indicators">
                       <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                       <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                       <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                   </ol>
                   <div class="carousel-inner">
                       <div class="item active">
                           <img src="http://placehold.it/900x500/39CCCC/ffffff&text=I+Love+Bootstrap" alt="First slide">

                           <div class="carousel-caption">
                               First Slide
                           </div>
                       </div>
                       <div class="item">
                           <img src="http://placehold.it/900x500/3c8dbc/ffffff&text=I+Love+Bootstrap" alt="Second slide">

                           <div class="carousel-caption">
                               Second Slide
                           </div>
                       </div>
                       <div class="item">
                           <img src="http://placehold.it/900x500/f39c12/ffffff&text=I+Love+Bootstrap" alt="Third slide">

                           <div class="carousel-caption">
                               Third Slide
                           </div>
                       </div>
                   </div>
                   <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                       <span class="fa fa-angle-left"></span>
                   </a>
                   <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                       <span class="fa fa-angle-right"></span>
                   </a>
               </div>
           </div>
           <div class="col-md-6" style="font-size: 16px;">
               Sed ut perspiciatis unde omnis iste
               natus error sit voluptatem accusantium
               doloremque laudantium, totam rem aperiam,
               eaque ipsa quae ab illo inventore veritatis et
               quasi architecto beatae vitae dicta sunt explicabo.
               Nemo enim ipsam voluptatem quia voluptas sit aspernatur
               aut odit aut fugit, sed quia consequuntur magni dolores
               eos qui ratione voluptatem sequi nesciunt. Neque porro
               quisquam est, qui dolorem ipsum quia dolor sit amet,
               consectetur, adipisci velit, sed quia non numquam eius
               modi tempora incidunt ut labore et dolore magnam aliquam
               quaerat voluptatem. Ut enim ad minima veniam, quis nostrum
               exercitationem ullam corporis suscipit laboriosam, nisi ut
               aliquid ex ea commodi consequatur? Quis autem vel eum iure
               reprehenderit qui in ea voluptate velit esse quam nihil molestiae
               consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?
           </div>
           <br>
       </div>
   </div>
@endsection