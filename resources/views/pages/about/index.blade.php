@extends('layouts.master')
@section('titleText')
    About {{env('SCHOOL_NAME')}}
@endsection
@section('header_bread')
    <h1 class="text-{{env('THEME')}}">
        About Us
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{route('welcome')}}"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">About</li>
    </ol>
    <br>
@endsection
@section('content')
    @include('pages.about.history')
    <hr>
    @include('pages.about.campuses')
@endsection