<div class="box box-{{env('SEARCH_BUTTON')}}">
    {{--<div class="box-header with-border">--}}
        {{--<h3 class="box-title">Notable Alumni</h3>--}}
    {{--</div>--}}
    <!-- /.box-header -->
    <div class="box-body">
        <h3 class="text-center">
            Welcome to Agape International School
            <hr>
        </h3>
        <p style="font-size: 16px; line-height: 1.5em;">
            Lorem Ipsum is simply dummy text of the
            printing and typesetting industry. Lorem
            Ipsum has been the industry's standard dummy
            text ever since the 1500s, when an unknown printer
            took a galley of type and scrambled it to make a
            type specimen book. It has survived not only five
            centuries, but also the leap into electronic typesetting,
            remaining essentially unchanged. It was popularised
            in the 1960s with the release of Letraset sheets containing
            Lorem Ipsum passages, and more recently with desktop
            publishing software like Aldus PageMaker including versions of Lorem Ipsum.
        </p>
    </div>
</div>