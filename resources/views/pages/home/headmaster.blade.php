<div class="box box-widget widget-user with-border">
    <!-- Add the bg color to the header using any of the bg-* classes -->
    <div class="widget-user-header 2bg-{{env('THEME')}}">
        <h3 class="widget-user-username">Alexander Pierce</h3>
        <h5 class="widget-user-desc">Founder &amp; Headmaster</h5>
    </div>
    <div class="widget-user-image">
        <img class="img-circle" src="{{asset('custom/dist/img/user1-128x128.jpg')}}" alt="User Avatar">
    </div>
    <div class="box-footer">
        <div class="row">
            <div class="col-sm-12 border-right">
                <div class="description-block">
                    <h3 class="text-{{env('SEARCH_BUTTON')}}">Welcome to {{env('SCHOOL_NAME', 'Agape')}}</h3>
                    {{--<hr>--}}
                    <blockquote style="font-size: 16px;">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore magna
                    </blockquote>
                </div>
                <!-- /.description-block -->
            </div>
        </div>
        <!-- /.row -->
    </div>
</div>