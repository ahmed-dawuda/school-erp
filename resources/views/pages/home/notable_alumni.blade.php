<div class="box box-{{env('SEARCH_BUTTON')}}">
    <div class="box-header with-border">
        <h3 class="box-title">Notable Alumni</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body no-padding">
        <ul class="users-list clearfix">
            <li>
                <img src="{{asset('custom/dist/img/user1-128x128.jpg')}}" alt="User Image">
                <a class="users-list-name" href="#">Alexander Pierce</a>
                <span class="users-list-date">Today</span>
            </li>
            <li>
                <img src="{{asset('custom/dist/img/user8-128x128.jpg')}}" alt="User Image">
                <a class="users-list-name" href="#">Norman</a>
                <span class="users-list-date">Yesterday</span>
            </li>
            <li>
                <img src="{{asset('custom/dist/img/user7-128x128.jpg')}}" alt="User Image">
                <a class="users-list-name" href="#">Jane</a>
                <span class="users-list-date">12 Jan</span>
            </li>
            <li>
                <img src="{{asset('custom/dist/img/user6-128x128.jpg')}}" alt="User Image">
                <a class="users-list-name" href="#">John</a>
                <span class="users-list-date">12 Jan</span>
            </li>
            <li>
                <img src="{{asset('custom/dist/img/user2-160x160.jpg')}}" alt="User Image">
                <a class="users-list-name" href="#">Alexander</a>
                <span class="users-list-date">13 Jan</span>
            </li>
            <li>
                <img src="{{asset('custom/dist/img/user5-128x128.jpg')}}" alt="User Image">
                <a class="users-list-name" href="#">Sarah</a>
                <span class="users-list-date">14 Jan</span>
            </li>
            <li>
                <img src="{{asset('custom/dist/img/user4-128x128.jpg')}}" alt="User Image">
                <a class="users-list-name" href="#">Nora</a>
                <span class="users-list-date">15 Jan</span>
            </li>
            <li>
                <img src="{{asset('custom/dist/img/user3-128x128.jpg')}}" alt="User Image">
                <a class="users-list-name" href="#">Nadia</a>
                <span class="users-list-date">15 Jan</span>
            </li>
        </ul>
        <!-- /.users-list -->
    </div>
    <!-- /.box-body -->
    <div class="box-footer text-center">
        <a href="javascript:void(0)" class="uppercase">View All Users</a>
    </div>
    <!-- /.box-footer -->
</div>