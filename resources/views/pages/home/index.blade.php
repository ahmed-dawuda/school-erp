@extends('layouts.master')
@section('titleText')
    Welcome to {{env("SCHOOL_NAME", "Agape")}}
@endsection
@section('css')
<style>
    #ui-buttons{
        padding-top: 20px;
    }
</style>
@endsection
@section('carousel')
<div id="ui-buttons">
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-level-up"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Get Directions</span>
                    {{--<span class="info-box-number">90<small>%</small></span>--}}
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-book"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Browse Library</span>
                    <span class="info-box-number">41,410+ Books</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-line-chart"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Academic</span>
                    <span class="info-box-number">A<sup>+</sup></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Our Population</span>
                    <span class="info-box-number">2,000</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>
</div>
    @include('pages.home.carousel')
@endsection
@section('header_bread')

@endsection
@section('content')
    <div class="row clearfix">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            @include('pages.home.headmaster')
            @include('pages.home.research_projects')
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            @include('pages.home.welcome')
            @include('pages.home.notable_alumni')
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            @include('pages.home.news')
            @include('pages.home.events')
        </div>
    </div>
@endsection
@section('js')

@endsection