@extends('layouts.master')
@section('titleText')
    Browse Questions
@endsection
@section('css')
    <style>
        .info-box-number{
            color: #808B96;
        }
    </style>
@endsection
@section('header_bread')
    <h1 class="text-{{env('THEME')}}">
        Browse Questions and answers
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{route('welcome')}}"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Question Bank</li>
    </ol>
    <br>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="#">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-search-plus"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"></span>
                        <span class="info-box-number">Kindergarten</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </a>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="#">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-search-plus"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"></span>
                        <span class="info-box-number">Primary Level Books</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </a>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="#">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-search-plus"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"></span>
                        <span class="info-box-number">Junior High</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </a>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="#">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-search-plus"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"></span>
                        <span class="info-box-number">Senior High</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </a>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="box box-{{env('SEARCH_BUTTON')}}">
                <div class="box-header with-border">
                    <h3 class="box-title">Recently added questions</h3>
                </div>
                <div class="row clearfix">
                    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                        <div class="box-body">
                            <ul class="products-list product-list-in-box">
                                <li class="item">
                                    <div class="product-img">
                                        <img src="{{asset('custom/dist/img/default-50x50.gif')}}" alt="Product Image">
                                    </div>
                                    <div class="product-info">
                                        <a href="javascript:void(0)" class="product-title">Samsung TV
                                            <span class="label label-warning pull-right">KG</span></a>
                    <span class="product-description">
                          Samsung 32" 1080p 60Hz LED Smart HDTV.
                        </span>
                                    </div>
                                </li>
                                <!-- /.item -->
                                <li class="item">
                                    <div class="product-img">
                                        <img src="{{asset('custom/dist/img/default-50x50.gif')}}" alt="Product Image">
                                    </div>
                                    <div class="product-info">
                                        <a href="javascript:void(0)" class="product-title">Bicycle
                                            <span class="label label-info pull-right">SHS</span></a>
                    <span class="product-description">
                          26" Mongoose Dolomite Men's 7-speed, Navy Blue.
                        </span>
                                    </div>
                                </li>
                                <!-- /.item -->
                                <li class="item">
                                    <div class="product-img">
                                        <img src="{{asset('custom/dist/img/default-50x50.gif')}}" alt="Product Image">
                                    </div>
                                    <div class="product-info">
                                        <a href="javascript:void(0)" class="product-title">Xbox One <span
                                                    class="label label-danger pull-right">JHS</span></a>
                    <span class="product-description">
                          Xbox One Console Bundle with Halo Master Chief Collection.
                        </span>
                                    </div>
                                </li>
                                <!-- /.item -->
                                <li class="item">
                                    <div class="product-img">
                                        <img src="{{asset('custom/dist/img/default-50x50.gif')}}" alt="Product Image">
                                    </div>
                                    <div class="product-info">
                                        <a href="javascript:void(0)" class="product-title">PlayStation 4
                                            <span class="label label-success pull-right">Primary</span></a>
                    <span class="product-description">
                          PlayStation 4 500GB Console (PS4)
                        </span>
                                    </div>
                                </li>
                                <!-- /.item -->
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection