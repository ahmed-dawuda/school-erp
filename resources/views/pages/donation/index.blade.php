@extends('layouts.master')
@section('titleText')
    Donate to help needy students | Donate Now
@endsection
@section('css')
    <style>
        .payment{
            box-shadow: 1px 1px 10px;
            padding: 20px;
            background: #D5F5E3;
            margin-bottom: 10px;
        }
        .selectedStudent{
            box-shadow: 1px 1px 10px;
            border-radius: 3em;
        }
    </style>
@endsection
@section('header_bread')
    <h1 class="text-{{env('THEME')}}">
        Donate to fund needy but brilliant students
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{route('welcome')}}"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Donations</li>
    </ol>
    <br>
@endsection
@section('content')
    <div class="row" id="donation">
        <div class="col-md-12">
            <div class="box box-{{env('SEARCH_BUTTON')}}">
                <div class="box-header with-border">
                    <h3 class="box-title">Please don't ignore, your cedi could shape someone's future</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="row clearfix">
                    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">

                    </div>
                    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                        <form role="form"  method="post" id="donationForm">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Please Select Your Title</label>
                                    <select class="form-control" name="title">
                                        <option>Title</option>
                                        <option>Mr.</option>
                                        <option>Mrs.</option>
                                        <option>Ms.</option>
                                        <option>Rv.</option>
                                        <option>Dr.</option>
                                        <option>Prof.</option>
                                    </select>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Surname <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control"  placeholder="e.g. Doe" name="surname">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Firstname <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control"  placeholder="e.g. John" name="firstname">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Your Email address <span class="text-danger">*</span></label>
                                    <input type="email" class="form-control"  placeholder="example@email.com" name="email">
                                </div>

                                <div class="form-group">
                                    <label>Your Mobile Number <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control"  placeholder="e.g. 0547406827" name="phone">
                                </div>

                                <div class="form-group">
                                    <label>Donate to a particular student</label>
                                    <span style="padding: 10px;">
                                        <input type="radio" name="for_student" value="0" checked id="no">
                                        <label for="no">Any needy student</label>
                                    </span>
                                    <span style="padding: 10px;">
                                        <input type="radio" name="for_student" value="1" id="yes">
                                        <label for="yes">Yes, donate to someone</label>
                                    </span>
                                </div>

                                <div class="form-group payment hidden" id="for_student">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Student's surname <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control"  placeholder="e.g. Doe" name="student_surname">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Student's firstname <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control"  placeholder="e.g. John" name="student_firstname">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Select Payment Method <span class="text-danger">*</span></label>
                                    <select class="form-control" id="payment_method" name="payment_method">
                                        <option value="0">Choose Method</option>
                                        <option value="mobile_money">Mobile Money</option>
                                        <option value="credit_card">Credit Card</option>
                                    </select>
                                </div>

                                <div class="form-group payment hidden" id="mobile_money">
                                    <label>Select Network <span class="text-danger">*</span></label>
                                    <select class="form-control" id="network" name="network_type">
                                        <option value="0">Choose Network</option>
                                        <option value="mtn">MTN</option>
                                        <option value="tigo">Tigo</option>
                                        <option value="vodafone">Vodafone</option>
                                        <option value="airtel">Airtel</option>
                                    </select>
                                </div>

                                <div class="payment hidden" id="credit_card">
                                    <div class="form-group">
                                        <label>Choose Card Type<span class="text-danger">*</span></label>
                                        <input type="radio" name="card_type" value="visa" checked> VISA
                                        <input type="radio" name="card_type" value="master_card">Master Card
                                    </div>

                                    <div class="form-group">
                                        <label >Card Number <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control"  placeholder="e.g. 54002646321640" name="card_number">
                                    </div>

                                    <div class="form-group">
                                        <label>Expiration Date <span class="text-danger">*</span></label>
                                        <div class="row clearfix">
                                            <div class="col-md-2">
                                                mm <input type="text" class="form-control"  placeholder="mm" name="month">
                                            </div>
                                            <div class="col-md-2">
                                                yy <input type="text" class="form-control"  placeholder="yy" name="year">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label >Card Security Code <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control"  placeholder="e.g. 123" name="card_security_code">
                                    </div>

                                    <div class="form-group">
                                        <label >Card Holder Name <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control"  placeholder="e.g. John Doe" name="card_holder_name">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label >Amount to donate (GHS) <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control"  placeholder="e.g. 100" name="amount">
                                </div>

                                <div class="form-group">
                                    <label>Any message for us? <span class="text-success">(Optional)</span></label>
                                    <textarea class="form-control" rows="3" placeholder="Type your message..." name="message"></textarea>
                                </div>

                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-{{env('SEARCH_BUTTON')}}">Apply</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="confirm_student">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Please select & confirm the student</h4>
                </div>
                <div class="modal-body">
                    <ul class="users-list clearfix">
                        <li>
                            <img src="{{asset('custom/dist/img/user1-128x128.jpg')}}" alt="User Image">
                            <a class="users-list-name" href="#">Alexander Pierce</a>
                            <span class="users-list-date">Today</span>
                        </li>
                        <li>
                            <img src="{{asset('custom/dist/img/user8-128x128.jpg')}}" alt="User Image">
                            <a class="users-list-name" href="#">Norman</a>
                            <span class="users-list-date">Yesterday</span>
                        </li>
                        <li>
                            <img src="{{asset('custom/dist/img/user7-128x128.jpg')}}" alt="User Image">
                            <a class="users-list-name" href="#">Jane</a>
                            <span class="users-list-date">12 Jan</span>
                        </li>
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><span class="fa fa-close"></span></button>
                    <button type="button" class="btn btn-{{env('SEARCH_BUTTON')}}">Confirm</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
@section('js')
    <script>
        $(document).ready(function (event)
        {
            $('#donation').on('change', '#payment_method', function (event)
            {
                var payment_method = $(this).val()
                if (payment_method == 'mobile_money') {
                    $('#mobile_money').removeClass('hidden')
                    $('#credit_card').addClass('hidden')
                } else if (payment_method == 'credit_card') {
                    $('#credit_card').removeClass('hidden')
                    $('#mobile_money').addClass('hidden');
                } else {
                    $('#credit_card').addClass('hidden')
                    $('#mobile_money').addClass('hidden');
                }
            })

            $('#donation').on('change', 'input[name="for_student"]', function (event)
            {
                if ($(this).val() == "0") {
                    $('#for_student').addClass('hidden')
                } else {
                    $('#for_student').removeClass('hidden')
                }
            })

            $('#donation').on('submit', '#donationForm', function(event)
            {
                event.preventDefault()
                setTimeout(function(){
                    $('#confirm_student').modal({
                        backdrop: "static"
                    })
                }, 1000)
            })

            $('.modal-body li').click(function(){
                $('.modal-body li').removeClass('selectedStudent')
                $(this).addClass('selectedStudent')
            })
        })
    </script>
@endsection