@extends('layouts.master')
@section('titleText')
    Admission | Apply Now
@endsection
@section('header_bread')
    <h1 class="text-{{env('THEME')}}">
        {{ucfirst($level)}} Level Application
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{route('welcome')}}"><i class="fa fa-home"></i> Home</a></li>
        <li>Admission</li>
        <li class="active">{{ucfirst($level)}}</li>
    </ol>
    <br>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-{{env('SEARCH_BUTTON')}}">
                <div class="box-header with-border">
                    <h3 class="box-title">Read before filling the form</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="row clearfix">
                    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">

                    </div>
                    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                        <form role="form" action="{{route('admission-create', ['level'=> $level])}}" method="post">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Full Name</label>
                                    <input type="text" class="form-control"  placeholder="Enter full name">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email address</label>
                                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                                </div>
                                <div class="form-group">
                                    <label>Message</label>
                                    <textarea class="form-control" rows="3" placeholder="Type your message..."></textarea>
                                </div>

                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" checked> Send me news and events
                                    </label>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-{{env('SEARCH_BUTTON')}}">Apply</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection