@extends('layouts.master')
@section('titleText')
    Browse Library
@endsection
@section('css')
    <style>
        .info-box-number{
            color: #808B96;
        }
        .nav-stacked li a{
            color: blue;
        }
        .selectedCat{
            border-right: 5px solid #2C3E50;
            background: #F2F4F4 !important;
            padding-left: 20px !important;
        }

    </style>
@endsection
@section('header_bread')
    <h1 class="text-{{env('THEME')}}">
        Browse library
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{route('welcome')}}"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Library</li>
    </ol>
    <br>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{route('library-level', ['level'=> 'kindergarten'])}}">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-search-plus"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"></span>
                        <span class="info-box-number">Kindergarten</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </a>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{route('library-level', ['level'=> 'primary-level'])}}">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-search-plus"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"></span>
                        <span class="info-box-number">Primary Level Books</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </a>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{route('library-level', ['level'=> 'junior-high'])}}">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-search-plus"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"></span>
                        <span class="info-box-number">Junior High</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </a>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{route('library-level', ['level'=> 'senior-high'])}}">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-search-plus"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"></span>
                        <span class="info-box-number">Senior High</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </a>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>
    @if(url()->current() == route('library'))
        @include('pages.library.recently')
    @else
        @include('pages.library.categories')
    @endif
@endsection
@section('js')
    <script>
        $(document).ready(function ()
        {
            $(document).on('click', '.item', function (event)
            {
                event.preventDefault()
                $('.item').removeClass('selectedCat')
                $(this).addClass('selectedCat')
            })

            $('.media').media();
        })
    </script>
@endsection