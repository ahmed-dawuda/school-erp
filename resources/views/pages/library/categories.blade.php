<div class="row">
    <div class="col-md-12">
        <div class="box box-{{env('SEARCH_BUTTON')}}">
            <div class="box-header with-border">
                <h3 class="box-title">Categories under {{ucfirst($level)}}</h3>
            </div>
            <div class="row clearfix">
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-6">
                    <div class="box-body">
                        <ul class="products-list product-list-in-box">
                            <li class="item selectedCat">
                                <div class="product-img">
                                    <img src="{{asset('custom/dist/img/default-50x50.gif')}}" alt="Product Image">
                                </div>
                                <div class="product-info">
                                    <a href="#" class="product-title">Science
                                        {{--<span class="label label-warning pull-right">KG</span></a>--}}
                    <span class="product-description">
                          Samsung 32" 1080p 60Hz LED Smart HDTV.
                        </span>
                                </div>
                            </li>
                            <!-- /.item -->
                            <li class="item">
                                <div class="product-img">
                                    <img src="{{asset('custom/dist/img/default-50x50.gif')}}" alt="Product Image">
                                </div>
                                <div class="product-info">
                                    <a href="#" class="product-title">Mathematics
                                        {{--<span class="label label-info pull-right">SHS</span></a>--}}
                    <span class="product-description">
                          26" Mongoose Dolomite Men's 7-speed, Navy Blue.
                        </span>
                                </div>
                            </li>
                            <!-- /.item -->
                            <li class="item">
                                <div class="product-img">
                                    <img src="{{asset('custom/dist/img/default-50x50.gif')}}" alt="Product Image">
                                </div>
                                <div class="product-info">
                                    <a href="#" class="product-title">Arts <span
                                                {{--class="label label-danger pull-right">JHS</span></a>--}}
                    <span class="product-description">
                          Xbox One Console Bundle with Halo Master Chief Collection.
                        </span>
                                </div>
                            </li>
                            <!-- /.item -->
                            <li class="item">
                                <div class="product-img">
                                    <img src="{{asset('custom/dist/img/default-50x50.gif')}}" alt="Product Image">
                                </div>
                                <div class="product-info">
                                    <a href="javascript:void(0)" class="product-title">Business
                                        {{--<span class="label label-success pull-right">Primary</span></a>--}}
                    <span class="product-description">
                          PlayStation 4 500GB Console (PS4)
                        </span>
                                </div>
                            </li>

                            <li class="item">
                                <div class="product-img">
                                    <img src="{{asset('custom/dist/img/default-50x50.gif')}}" alt="Product Image">
                                </div>
                                <div class="product-info">
                                    <a href="javascript:void(0)" class="product-title">Religion
                                        {{--<span class="label label-success pull-right">Primary</span></a>--}}
                                        <span class="product-description">
                          PlayStation 4 500GB Console (PS4)
                        </span>
                                </div>
                            </li>

                            <li class="item">
                                <div class="product-img">
                                    <img src="{{asset('custom/dist/img/default-50x50.gif')}}" alt="Product Image">
                                </div>
                                <div class="product-info">
                                    <a href="javascript:void(0)" class="product-title">Information Communications Technology
                                        {{--<span class="label label-success pull-right">Primary</span></a>--}}
                                        <span class="product-description">
                          PlayStation 4 500GB Console (PS4)
                        </span>
                                </div>
                            </li>

                            <li class="item">
                                <div class="product-img">
                                    <img src="{{asset('custom/dist/img/default-50x50.gif')}}" alt="Product Image">
                                </div>
                                <div class="product-info">
                                    <a href="javascript:void(0)" class="product-title">Design
                                        {{--<span class="label label-success pull-right">Primary</span></a>--}}
                                        <span class="product-description">
                          PlayStation 4 500GB Console (PS4)
                        </span>
                                </div>
                            </li>

                            <li class="item">
                                <div class="product-img">
                                    <img src="{{asset('custom/dist/img/default-50x50.gif')}}" alt="Product Image">
                                </div>
                                <div class="product-info">
                                    <a href="javascript:void(0)" class="product-title">Story Books
                                        {{--<span class="label label-success pull-right">Primary</span></a>--}}
                                        <span class="product-description">
                          PlayStation 4 500GB Console (PS4)
                        </span>
                                </div>
                            </li>
                            <!-- /.item -->
                        </ul>
                    </div>
                </div>
                <div class="col-md-8 col-lg-8 col-sm-6 col-xs-6">
                    <div class="box-footer no-padding">
                        <ul class="nav nav-stacked">
                            <li><a href="{{asset('files/ccna.pdf')}}" target="parent" style="color: #1B4F72; font-weight: bold;">Projects</a></li>
                            <li><a href="{{asset('files/practice_book_lit.pdf')}}" target="parent" style="color: #1B4F72; font-weight: bold;">Tasks</a></li>
                            <li><a href="#" style="color: #1B4F72; font-weight: bold;">Completed Projects</a></li>
                            <li><a href="#" style="color: #1B4F72; font-weight: bold;">Followers</a></li>
                            <li><a href="#" style="color: #1B4F72; font-weight: bold;">Projects</a></li>
                            <li><a href="#" style="color: #1B4F72; font-weight: bold;">Tasks</a></li>
                            <li><a href="#" style="color: #1B4F72; font-weight: bold;">Completed Projects</a></li>
                            <li><a href="#" style="color: #1B4F72; font-weight: bold;">Followers</a></li>
                            <li><a href="#" style="color: #1B4F72; font-weight: bold;">Projects</a></li>
                            <li><a href="#" style="color: #1B4F72; font-weight: bold;">Tasks</a></li>
                            <li><a href="#" style="color: #1B4F72; font-weight: bold;">Completed Projects</a></li>
                            <li><a href="#" style="color: #1B4F72; font-weight: bold;">Followers</a></li>
                            <li><a href="#" style="color: #1B4F72; font-weight: bold;">Projects</a></li>
                            <li><a href="#" style="color: #1B4F72; font-weight: bold;">Tasks</a></li>
                            <li><a href="#" style="color: #1B4F72; font-weight: bold;">Completed Projects</a></li>
                            <li><a href="#" style="color: #1B4F72; font-weight: bold;">Followers</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="reader">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="height: 100%;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Default Modal</h4>
            </div>
            <div class="modal-body">
{{--                <iframe src="{{asset('files/ccna.pdf')}}"></iframe>--}}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>