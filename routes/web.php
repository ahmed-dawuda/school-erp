<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('welcome');

Route::group(['prefix'=> 'about'], function()
{
    Route::get('/history', 'AboutController@history')->name('history');
    Route::get('/campuses', 'AboutController@campuses')->name('campuses');
    Route::get('/map_direction', 'AboutController@mapsDirection')->name('map');
    Route::get('/students', 'AboutController@students')->name('students');
    Route::get('/staff', 'AboutController@staff')->name('staff');
    Route::get('/alumni', 'AboutController@alumni')->name('alumni');
    Route::get('/academics', 'AboutController@academics')->name('academics');
    Route::get('/network', 'AboutController@network')->name('network');
});

Route::get('/admissions/{level}', 'AdmissionController@index')->name('admission');
Route::post('/admissions/{level}', 'AdmissionController@create')->name('admission-create');
Route::get('/donations', 'DonationController@index')->name('donation');
Route::get('/downloads', 'DownloadController@index')->name('download');

Route::group(['prefix'=> 'library'], function ()
{
    Route::get('/', 'LibraryController@index')->name('library');
    Route::get('/{level}', 'LibraryController@subLevel')->name('library-level');
});

Route::group(['prefix'=> 'questions'], function ()
{
    Route::get('/', 'QuestionBankController@index')->name('questions');
});