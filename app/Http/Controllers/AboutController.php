<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function index(Request $request)
    {
        return view('pages.about.index');
    }

    public function history(Request $request)
    {
        return view('pages.about.history');
    }

    public function campuses(Request $request)
    {
        return view('pages.about.campuses');
    }

    public function mapsDirection(Request $request)
    {
        return view('pages.about.map_direction');
    }

    public function students(Request $request)
    {
        return view('pages.about.students');
    }

    public function staff(Request $request)
    {
        return view('pages.about.staff');
    }

    public function alumni(Request $request)
    {
        return view('pages.about.alumni');
    }

    public function  academics(Request $request)
    {
        return view('pages.about.academics');
    }

    public function network(Request $request)
    {
        return view('pages.about.network');
    }
}
