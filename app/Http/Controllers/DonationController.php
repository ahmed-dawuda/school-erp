<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DonationController extends Controller
{
    public function index(Request $request)
    {
        return view('pages.donation.index');
    }
}
