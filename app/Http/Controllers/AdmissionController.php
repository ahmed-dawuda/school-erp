<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdmissionController extends Controller
{
    public function index(Request $request, $level = null)
    {
        if ($level == null) {
            return redirect()->back();
        }

        return view('pages.admission.index', ['level'=> $level]);
    }

    public function create(Request $request, $level = null)
    {
        if ($level == null) {
            return redirect()->back();
        }
        return;
    }
}
