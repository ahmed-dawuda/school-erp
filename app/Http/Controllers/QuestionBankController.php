<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class QuestionBankController extends Controller
{
    public function index(Request $request)
    {
        return view('pages.questions.index');
    }
}
