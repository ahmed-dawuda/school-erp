<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LibraryController extends Controller
{
    public function index(Request $request)
    {
        return view('pages.library.index');
    }

    public function subLevel(Request $request, $level)
    {
        return view('pages.library.index', ['level'=> $level]);
    }
}
